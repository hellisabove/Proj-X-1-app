#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>
using namespace std;

int main()
{
    cout << "Welcome to Project X-1" << endl;
    cout << "Please enter Credentials: ";
    string name;
    cin >> name;
CRED:

    cout << "Choose what you want to do: " << endl;
    cout << "[1] About Us" << endl << "[2] Pwnagotchi" << endl << "[3] Hidden" << endl;
    cout << "[4] Quit" << endl;
    string input;
    getline(cin, input);
    cin >> input;
    if(input == "1") {
        cout << "We are a group created by HellIsAbove who's objectives are unknown for anyone outside of us"<<endl;
        int c = getchar();
        cout<<endl;
        goto CRED;
    }
    
    if(input == "2") {
        char *command = "xdg-open https://pwnagotchi.ai";
    	system(command);
    }
    
    if(input == "3") {
        cout<<"Our Hidden Domain is currently under construction. You will be notified when it's done";
        cout<<endl;
        goto CRED;
    }

    if(input == "4") {
        exit(0);
    }

    else {
        cout<<"Invalid input";
        cout<<endl;
        goto CRED;
    }

}
